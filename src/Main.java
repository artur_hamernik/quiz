public class Main {

    public static void main(String[] args){
        Quiz quiz = new QuizImpl();
        int first = Quiz.MIN_VALUE;
        int last = Quiz.MAX_VALUE;
        int digit = (first+last) / 2; // zainicjuj zmienna

        for(int counter = 1; ;counter++) {

            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;
            } catch (Quiz.ParamTooLarge paramTooLarge) {
                System.out.println("Argument za duzy!!!");
                last = digit;
                digit = (first+last)/2;
            } catch (Quiz.ParamTooSmall paramTooSmall) {
                System.out.println("Argument za maly!!!");
                first = digit;
                digit = (first+last)/2;
            }
        }
    }
}
